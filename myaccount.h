#ifndef MYACCOUNT_H
#define MYACCOUNT_H

#include <iostream>

#include <pjsua2.hpp>
using namespace pj;

class MyAccount : public pj::Account {
public:
    ~MyAccount();
    virtual void onRegState(OnRegStateParam& prm) override;
    virtual void onIncomingCall(OnIncomingCallParam &iprm) override;

    // Account interface
public:
    virtual void onIncomingSubscribe(OnIncomingSubscribeParam &prm) override;
    virtual void onInstantMessage(OnInstantMessageParam &prm) override;
};

#endif // MYACCOUNT_H
