#include <FelgoApplication>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <pjsua2.hpp>
#include <iostream>

#include "mybuddy.h"
#include "myaccount.h"

using namespace pj;

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    FelgoApplication felgo;
    felgo.setPreservePlatformFonts(true);

    // QQmlApplicationEngine is the preferred way to start qml projects since Qt 5.2
    // if you have older projects using Qt App wizards from previous QtCreator versions than 3.1, please change them to QQmlApplicationEngine
    QQmlApplicationEngine engine;
    felgo.initialize(&engine);

    // use this instead of the below call for faster deployment on desktop when you comment the RESOURCES in the .pro file (this avoids compiling all your resources into the binary and has shorter compile times; also it allows re-running the same project in non-shadow builds)
    // this is the preferred deployment option during development on Desktop (for deployment on mobile use the qrc approach below)
    felgo.setMainQmlFileName(QStringLiteral("qml/MessagingMain.qml"));

    // use this instead of the above call to avoid deployment of the qml files and compile them into the binary with qt's resource system qrc
    // this is the preferred deployment option for debug and release builds on mobile, and for publish builds on all platforms
    // to avoid deployment of your qml files and images, also comment the DEPLOYMENTFOLDERS command in the .pro file
    // only use the above non-qrc approach, during development on desktop
    //  felgo.setMainQmlFileName(QStringLiteral("qrc:/qml/MessagingMain.qml"));

    Endpoint ep;

    ep.libCreate();

    // Initialize endpoint
    EpConfig ep_cfg;
    ep_cfg.logConfig.level = 4;
    ep_cfg.uaConfig.maxCalls = 4;
    ep_cfg.medConfig.sndClockRate = 16000;
    ep_cfg.uaConfig.userAgent;
    ep.libInit(ep_cfg);

    // Create SIP transport. Error handling sample is shown
    TransportConfig tcfg;
    tcfg.port = 15060;
    // Optional, set CA/certificate/private key files.
    // tcfg.tlsConfig.CaListFile = "ca.crt";
    // tcfg.tlsConfig.certFile = "cert.crt";
    // tcfg.tlsConfig.privKeyFile = "priv.key";
    // Optional, set ciphers. You can select a certain cipher/rearrange the order of ciphers here.
     tcfg.tlsConfig.ciphers = ep.utilSslGetAvailableCiphers();
    try {
         // ep.transportCreate(PJSIP_TRANSPORT_TLS, tcfg);
         ep.transportCreate(PJSIP_TRANSPORT_UDP, tcfg);
    }
    catch (Error & err) {
        std::cout << err.info() << std::endl;
        return 1;
    }

    // Start the library (worker threads etc)
    ep.libStart();
    std::cout << "*** PJSUA2 STARTED ***" << std::endl;

    AccountConfig acc_cfg;
    acc_cfg.idUri = "sip:192.168.1.7:15060";
//    acc_cfg.regConfig.registrarUri = "sip:pjsip.org";
//    acc_cfg.sipConfig.authCreds.push_back( AuthCredInfo("digest", "*", "test1", 0, "secret1") );

    MyAccount *acc = new MyAccount;
    try {
        acc->create(acc_cfg);
    } catch(Error& err) {
        std::cout << "Account creation error: " << err.info() << std::endl;
    }

    MyBuddy buddy;

    engine.rootContext()->setContextProperty("buddy", &buddy);
    engine.load(QUrl(felgo.mainQmlFileName()));



    // Here we don't have anything else to do..
    //    pj_thread_sleep(10000);

    // Delete the account. This will unregister from server
    delete acc;

    return app.exec();
}
