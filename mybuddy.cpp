#include "mybuddy.h"

MyBuddy::MyBuddy()
{
    pj::AccountConfig acfg;
    acfg.idUri = "sip:192.168.1.7:15060";
    acc.create(acfg);

    pj::BuddyConfig cfg;
    cfg.uri = "sip:192.168.1.7:5060";
    create(acc, cfg);
}

void MyBuddy::sendMessage(QString txt) {
    pj::SendInstantMessageParam msgPr;
    msgPr.content = "QML" + txt.toStdString();
    sendInstantMessage(msgPr);
}
