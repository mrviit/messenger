#include "mycall.h"


MyCall::MyCall(Account &acc, int call_id)
    : Call(acc, call_id)
{ }

MyCall::~MyCall()
{ }

void MyCall::onCallState(OnCallStateParam &prm)
{

}

void MyCall::onCallMediaState(OnCallMediaStateParam &prm)
{

}


void MyCall::onCallTransferRequest(OnCallTransferRequestParam &prm)
{
}


void MyCall::onInstantMessage(OnInstantMessageParam &prm)
{
}
