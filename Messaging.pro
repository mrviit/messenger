# allows to add DEPLOYMENTFOLDERS and links to the Felgo library and QtCreator auto-completion
CONFIG += felgo
DEFINES += PJ_WIN32=1

INCLUDEPATH +=  \
    D:/Vi/pjsip2/pjproject/pjlib/include \
    D:/Vi/pjsip2/pjproject/pjsip/include \
    D:/Vi/pjsip2/pjproject/pjlib-util/include \
    D:/Vi/pjsip2/pjproject/pjmedia/include \
    D:/Vi/pjsip2/pjproject/pjnath/include

#DEPENDPATH += D:/Vi/pjsip2/pjproject/pjsip/include

LIBS += \
    -LD:\Vi\pjsip2\pjproject\lib    \
    -LC:\OpenSSL_Win64\lib      \
    -LD:\Vi\pjsip2\pjproject\pjsip\lib \
    -llibpjproject-x86_64-x64-vc14-Debug-Dynamic    \
    -lpjsua2-lib-x86_64-x64-vc14-Debug-Dynamic      \
    -lwsock32 -lws2_32 -lole32 -ldsound -lIphlpapi -lmswsock -lodbc32 -lodbccp32 -luser32


qmlFolder.source = qml
DEPLOYMENTFOLDERS += qmlFolder # comment for publishing

assetsFolder.source = assets
DEPLOYMENTFOLDERS += assetsFolder

# Add more folders to ship with the application here

RESOURCES += \
#    resources_messaging.qrc # uncomment for publishing

# NOTE: for PUBLISHING, perform the following steps:
# 1. comment the DEPLOYMENTFOLDERS += qmlFolder line above, to avoid shipping your qml files with the application (instead they get compiled to the app binary)
# 2. uncomment the resources.qrc file inclusion and add any qml subfolders to the .qrc file; this compiles your qml files and js files to the app binary and protects your source code
# 3. change the setMainQmlFile() call in main.cpp to the one starting with "qrc:/" - this loads the qml files from the resources
# for more details see the "Deployment Guides" in the Felgo Documentation

# during development, use the qmlFolder deployment because you then get shorter compilation times (the qml files do not need to be compiled to the binary but are just copied)
# also, for quickest deployment on Desktop disable the "Shadow Build" option in Projects/Builds - you can then select "Run Without Deployment" from the Build menu in Qt Creator if you only changed QML files; this speeds up application start, because your app is not copied & re-compiled but just re-interpreted


# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    myaccount.cpp \
    mybuddy.cpp \
    mycall.cpp

android {
  ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
  OTHER_FILES += android/AndroidManifest.xml
}

ios {
  QMAKE_INFO_PLIST = ios/Project-Info.plist
  OTHER_FILES += $$QMAKE_INFO_PLIST
}

HEADERS += \
    myaccount.h \
    mybuddy.h \
    mycall.h
