#ifndef MYCALL_H
#define MYCALL_H

#include <pjsua2.hpp>

using namespace pj;

class MyCall: public Call
{
public:
    MyCall(pj::Account &acc, int call_id = PJSUA_INVALID_ID);

    ~MyCall();

    // Notification when call's state has changed.
    virtual void onCallState(OnCallStateParam &prm) override;

    // Notification when call's media state has changed.
    virtual void onCallMediaState(OnCallMediaStateParam &prm) override;

    // Call interface
public:
    virtual void onCallTransferRequest(OnCallTransferRequestParam &prm) override;

    // Call interface
public:
    virtual void onInstantMessage(OnInstantMessageParam &prm) override;
};

#endif // MYCALL_H
