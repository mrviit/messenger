#ifndef MYBUDDY_H
#define MYBUDDY_H

#include <QObject>
#include <pjsua2.hpp>

#include "myaccount.h"

class MyBuddy : public QObject, public pj::Buddy
{
    Q_OBJECT
public:
    MyBuddy();
    ~MyBuddy() {}

    virtual void onBuddyState() {}

signals:

public slots:
    void sendMessage(QString txt);
private:
    MyAccount acc;
};

#endif // MYBUDDY_H
