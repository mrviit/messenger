#include "myaccount.h"
#include "mycall.h"

MyAccount::~MyAccount()
{
    // Invoke shutdown() first..
    shutdown();
}

void MyAccount::onRegState(pj::OnRegStateParam &prm) {
    pj::AccountInfo ai = getInfo();
    std::cout << "MyAccount MyAccountMyAccountMyAccountMyAccount" << std::endl;
    std::cout << (ai.regIsActive? "*** Register:" : "*** Unregister:")
              << " code=" << prm.code << std::endl;
}

void MyAccount::onIncomingCall(pj::OnIncomingCallParam &iprm)
{
    Call *call = new MyCall(*this, iprm.callId);

    // Just hangup for now
    CallOpParam op;
    op.statusCode = PJSIP_SC_DECLINE;
    call->hangup(op);

    // And delete the call
    delete call;
}


void MyAccount::onIncomingSubscribe(OnIncomingSubscribeParam &prm)
{
    std::cout << __FUNCTION__ << std::endl;
}

void MyAccount::onInstantMessage(OnInstantMessageParam &prm)
{
    std::cout << __FUNCTION__ << std::endl;
}
